import { createSlice } from '@reduxjs/toolkit'
import axios from 'axios'

export const counterSlice = createSlice({
  name: 'github',
  initialState: {
    user: {},
    repo: [],
  },
  reducers: {
    userSearch: (state, action) => {
      state.user = action.payload
    },
    repoSearch: (state, action) => {
      state.repo = action.payload
    }
  }
})

export const postUserSearchAsync = (data) => dispatch => {
  axios.get(`https://api.github.com/users/${data.text}?access_token=20e1e893fbae44a4e3e51037cdadcf78631a5a98`)
    .then(res => {
      dispatch(userSearch(res.data))
    })  
}

export const repoUserSearchAsync = (data) => dispatch => {
  axios.get(`https://api.github.com/users/${data}/repos?access_token=20e1e893fbae44a4e3e51037cdadcf78631a5a98`)
  .then(res => {
    dispatch(repoSearch(res.data))
  })
}

export const { userSearch, repoSearch } = counterSlice.actions

export default counterSlice.reducer