import { configureStore } from '@reduxjs/toolkit';
import counterSlice from './reducers/reducer';

export default configureStore({
    reducer: {
        counter: counterSlice
    }
});