
export const { } = globalStateSlice.actions;

export const getPokemonsAsync = () => dispatch => {

   axios.get(`https://pokeapi.co/api/v2/pokemon/`)
      .then(res => {
         dispatch(setPokemons(res.data));
      })

};