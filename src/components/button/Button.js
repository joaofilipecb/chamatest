import React from 'react'
import { ButtonStyle } from './ButtonStyle'

export const Button = (props) => {

    const clickUserProps = () => props.event(true)

    return (
       <ButtonStyle onClick={() => clickUserProps()}>{props.text}</ButtonStyle>
    )

}