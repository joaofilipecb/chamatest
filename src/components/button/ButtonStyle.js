import styled from 'styled-components'

const ButtonStyle = styled.button`
background: #406ae0;
border:0;
color:#fff;
width:200px;
border-radius: 100px;
cursor:pointer;
outline:none;
padding:10px;
z-index:1;
`

export { ButtonStyle }