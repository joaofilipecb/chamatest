import React from 'react'
import logo from '../../logo.svg'
import { HeaderStyle, Brand, Navigation } from './HeaderStyle'
import { Link } from 'react-router-dom'

export const Header = (props) => {
    return (
        <HeaderStyle>
            <Brand><img alt="logo" src={logo} /></Brand>
            <Navigation>
                {props.info.map((value) => <Link key={value.id} to={value.url}>{value.name}</Link>) }
            </Navigation>
        </HeaderStyle>
    )
}