import styled from 'styled-components'
const HeaderStyle = styled.nav`
    width:100%;
    background:#406ae0;
    padding-top:20px;
    padding-bottom:20px;
    display:flex;
    justify-content: space-between;
`
const Brand = styled.a`
    font-size: 1.5em;
    text-align: center;
    margin-left:20px;
`;

const Navigation = styled.div`
    margin-right:30px;
    margin-top:5px;
    a{
        text-decoration:none;
        color:#fff;
        padding:7px;
        text-transform:uppercase;
    }
`

export { HeaderStyle, Brand, Navigation }