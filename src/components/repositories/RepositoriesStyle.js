import styled from 'styled-components'

const RepositoriesStyle = styled.div`
    max-width:400px;
    margin-left:auto;
    margin-right:auto;
    margin-top:50px;
    transition: margin-top 1s;
`

export {RepositoriesStyle}