import React from 'react'
import { RepositoriesStyle } from './RepositoriesStyle'

export const Repositories = (props) => {
    return (
        <RepositoriesStyle>
            <h3>{props.info.name}</h3>
            <p>{props.info.description}</p>
        </RepositoriesStyle>
    )
}