import React from 'react'
import styled from 'styled-components'

const InputStyle = styled.input`
border:none !important;
outline:none;
border-radius:99px;
padding:20px;
max-width:500px;
width:100%;
display:block;
font-weight:400;
transform: translate(55px);
z-index:0;
`
export const Input = (props) => {
    
    const changeUser = (e) => props.event(e)

    return (
        <InputStyle onChange={(e) => changeUser(e)} placeholder="Nome do usuário"></InputStyle>
    )

}