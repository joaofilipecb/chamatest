import React, { useState } from 'react'
import { useDispatch } from 'react-redux'
import { Input } from '../input/Input'
import { Button } from '../button/Button'
import { postUserSearchAsync } from '../../store/reducers/reducer'
import { SearchStyle } from './SearchStyle'

export const Search = () => {

    const [search, setSearch] = useState()
    const dispatch = useDispatch()

    const dispatchSearch = () => {
        search && (
            dispatch(postUserSearchAsync(search))
        )
    }

    return (
        <SearchStyle>
            <Input event={data => setSearch({ text: data.target.value })}></Input>
            <Button id="btn-how-to-choose-provider" event={() => dispatchSearch()} text="Pesquisar"></Button>
        </SearchStyle>
    )
}