import styled from 'styled-components'

const SearchStyle = styled.div`
    background-color: rgb(235, 115, 56);
    padding:40px;
    display:flex;
    justify-content: center;
`

export { SearchStyle }