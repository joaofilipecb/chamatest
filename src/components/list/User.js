import React from 'react'
import { TitleUser } from './ListStyle'

export const User = (props) => {
    return (
        <div style={{overflow:'auto'}}>
            <TitleUser>{props.info.name}</TitleUser>
            <p>{props.info.location}</p>
        </div>
    )
}