import React from 'react'
import { User } from './User'
import { ListStyle, Perfil, TitleGiant } from './ListStyle'

export const List = (props) => {
    return (
        <ListStyle>
            <Perfil>
                <img alt="avatar" src={props.info.avatar_url} />
            </Perfil>
            <User info={props.info}></User>
            <TitleGiant>Repositórios</TitleGiant>
        </ListStyle>
    )
}