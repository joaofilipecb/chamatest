import styled from 'styled-components'

const ListStyle = styled.div`
    max-width:400px;
    margin-left:auto;
    margin-right:auto;
    margin-top:50px;
    transition: margin-top 1s;
`
const TitleGiant = styled.h2`
    margin-top:60px;
    text-transform:uppercase;
`

const TitleUser = styled.h2`
    font-size:20px;
    text-transform:uppercase;
    margin-bottom:0px;
`

const Perfil = styled.div`
    object-fit:cover;
    border-radius:100%;
    width:130px;
    height:100px;
    float:left;
    img{
        object-fit:cover;
        border-radius:100%;
        width:100px;
        height:100px;
    }
`

export {ListStyle, Perfil, TitleGiant, TitleUser}