import React from 'react'
import App from '../pages/app/App'
import History from '../pages/history/History'
import { BrowserRouter, Switch, Route } from 'react-router-dom'

export default function Routes() {
    return (
        <div>
            <BrowserRouter>
                <Switch>
                    <Route path="/" exact={true} component={App} />
                    <Route path="/history" component={History} />
                </Switch>
            </BrowserRouter>
        </div>
    )
}