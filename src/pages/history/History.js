import React from 'react'
import { Link } from "react-router-dom"
import { useDispatch } from 'react-redux'
import { postUserSearchAsync } from '../../store/reducers/reducer'
import { HistoryStyle } from './HistoryStyle'
import { Header } from '../../components/header/Header'

export default function History() {

  const dispatch = useDispatch()

  const showHistory = () => {
    return JSON.parse(localStorage.getItem('history'))
  }

  const newSearch = (data) => {
    dispatch(postUserSearchAsync({ text: data }))
  }

  return (
    <>
      <Header info={[{ id: 1, name: 'Home', url: '/' }, { id: 2, name: 'Histórico', url: 'history' }]}></Header>
      <HistoryStyle>
        {!showHistory() ? <h1>Não há pesquisas recentes, faça uma nova!</h1> : <h1>Pesquisas recentes</h1>}
        {showHistory() && showHistory().reverse().map((data, id) =>
          <div key={id}>
            <Link onClick={() => newSearch(data.value)} to="/" key={id}>{data.value}</Link>
          </div>
        )}
      </HistoryStyle>
    </>
  )
}