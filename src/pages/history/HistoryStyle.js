import styled from 'styled-components'
export const HistoryStyle = styled.div`
  max-width:400px;
  margin-left:auto;
  margin-right:auto;
  margin-top:50px;
  transition: margin-top 1s;
  a{
    display:block;
    font-size:25px;
    text-decoration:none;
    text-transform:uppercase;
    color:rgb(235,115,56);
    margin-top:10px;
    margin-bottom:10px;
  }
    h1{
      text-transform:uppercase;
      color:#406ae0;
    }
`

