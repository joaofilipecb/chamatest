import styled from 'styled-components'

const LoadingStyle = styled.img`
    position:absolute;
    margin-left:50%;
    margin-top:25%;
    top:-60px;
    left:-60px;
`

export default LoadingStyle