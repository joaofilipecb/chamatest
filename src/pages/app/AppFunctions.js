const addHistory = (value) => {
    let parse
    let spread
    if (!localStorage.getItem('history')) {
        parse = JSON.stringify([{ value, date: Date.now()}])
        localStorage.setItem('history', parse)
    } else {
        parse = JSON.parse(localStorage.getItem('history'))
        spread = [...parse, { value, date: Date.now() }]
        setTimeout(() => {
            parse = JSON.stringify(spread)
            localStorage.setItem('history', parse)
        }, 100)
    }
}

export { addHistory }