import React, { useState, useEffect } from 'react'
import { useSelector, useDispatch } from 'react-redux'
import { Search } from '../../components/search/Search'
import { List } from '../../components/list/List'
import { repoUserSearchAsync } from '../../store/reducers/reducer'
import { addHistory } from '../app/AppFunctions'
import { Repositories } from '../../components/repositories/Repositories'
import loadingSvg from '../../components/loading/loading.svg'
import LoadingStyle from './LoadingStyle'
import { Header } from '../../components/header/Header'

export default function App() {

  const selector = useSelector(state => state)
  const dispatch = useDispatch()
  const [loading, setLoading] = useState(true)
  const objectKeyUser = Object.keys(selector.counter.user).length
  const objectKeyRepo = Object.keys(selector.counter.repo).length
  const userInfo = selector.counter.user

  const loadingState = () => objectKeyUser >= 1 && loading ? <LoadingStyle src={loadingSvg} /> : showUserRepo()

  const showUserRepo = () => {
    let html
    if (objectKeyUser >= 1 && objectKeyRepo >= 1) {
      html = <><List info={userInfo}></List>
        {selector.counter.repo.map((value) =>
          <Repositories key={value.id} info={value}></Repositories>
        )}</>
    } else if (objectKeyUser >= 1) {
      html = <List info={selector.counter.user}></List>
    }
    return html
  }

  useEffect(() => {
    if (objectKeyUser >= 1) {
      dispatch(repoUserSearchAsync(userInfo.login))
      setLoading(true)
      addHistory(userInfo.login)
    }
  }, [selector.counter.user])

  useEffect(() => selector.counter.repo.length > 0 && setLoading(false), [selector.counter.repo])

  return (
    <>
      <Header info={[{ id: 1, name: 'Home', url: '/' }, { id: 2, name: 'Histórico', url: 'history' }]}></Header>
      <Search></Search>
      {loadingState()}
    </>
  )
}