# ChamaTest

Desenvolvimento de aplicação (front-end) para a CHAMA. A aplicação consiste em: ***pesquisar***, ***detalhar*** e ***listar*** usuários do github.

### Principais Bibliotecas utilizadas
 - styled-components
 - react-redux 
 - react-router-dom 
 - jest
 - redux
 - axios
### Instalação e iniciação
Após ter dado um clone, a partir do repositório: https://bitbucket.org/joaofilipecb/chamatest/src/master/, entre no diretório da aplicação e execute os seguintes comandos:

 - `npm install`

 - `npm start`